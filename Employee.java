import java.util.Scanner;

public class Employee {
private int empId;
private String empName;
private float empSalary;


    public int getId()
    {return empId;}

    public String getEmpName()
    {return empName;}

    public  float getSalary()
    {
        return empSalary;
    }
    public void setId(int id)
    {
        this.empId= id;
    }
    public void setName(String name)
    {
        this.empName= name;
    }
    public void setSalary(float salary)
    {
        this.empSalary= salary;
    }

    public Employee( int id, String empName, float salary)
    {
        this.empId = id;
        this.empName = empName;
        this.empSalary = salary;
    }

//public boolean equals(object obj)
//{
//    Employee emp = new Employee();
//
//    return false;
//}
    public static void main(String[] args)
    {
        Employee emp1 = new Employee(101,"Prathima",2000);
        Employee emp2 = new Employee(102,"Saritha",1000);

        if(emp1.empSalary == emp2.empSalary)
            System.out.println("Salary is equal");
        else
            System.out.println("Salary is  not equal");
    }

}
