public class Manager extends  Employee {
    private  float mgrBonus;
    public Manager(int id, String empName, float salary,float bonus) {
        super(id, empName, salary);
        this.mgrBonus = bonus;
    }
}
