public class BasicJava {
    public static void main(String[] args) {

        //palindrome number or not
        int num = 121, rvInteger = 0, remainder, originalInteger;
        originalInteger = num;
        // reversed integer is stored in variable
        while( num != 0 )
        {
            remainder = num % 10;
            rvInteger = rvInteger * 10 + remainder;
            num  /= 10;
        }
        // palindrome if orignalInteger and reversedInteger are equal
        if (originalInteger == rvInteger)
            System.out.println(originalInteger + " is a palindrome.");
        else
            System.out.println(originalInteger + " is not a palindrome.");


        //fibonacci series

        int input = 10, t1 = 0, t2 = 1;
        System.out.print("First " + input + " terms: ");
        for (int i = 1; i <= input; ++i)
        {
            System.out.print(t1 + " , ");
            int sum = t1 + t2;
            t1 = t2;
            t2 = sum;
        }
        System.out.println("\n");
// Is Prime or not
        int number = 29;
        boolean flag = false;
        for(int i = 2; i <= number/2; ++i)
        {
            // condition for nonprime number
            if(number % i == 0)
            {
                flag = true;
                break;
            }
        }
        if (!flag)
            System.out.println(number + " is a prime number.");
        else
            System.out.println(number + " is not a prime number.");


        //Factorial

        //Factorial
        int integer = 10;
        long factorial = 1;

        for(int it = 1; it <= integer; ++it)
        {
            // factorial = factorial * i;
            factorial *= it;
        }
        System.out.println("Factorial of "+ integer +" is :"+ factorial);
    }


}

