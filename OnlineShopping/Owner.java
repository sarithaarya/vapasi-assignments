import java.util.*;


public class Owner {

    private static HashSet<String> brands = new HashSet<String>();

    private  final Scanner scr = new Scanner(System.in);
    public   HashMap<String, ArrayList<String>> brandCategories = new HashMap<String,ArrayList<String>>();
    public   HashMap<String, ArrayList<String>> brandSubCategories = new HashMap<String,ArrayList<String>>();
    public   HashMap<String,HashMap<String, ArrayList<String>>> items = new HashMap<String,HashMap<String, ArrayList<String>>>();

    public   HashSet<String> addBrands(){

        System.out.println("Enter brand name");


        while(scr.hasNext()){
            String str = scr.nextLine();
            if(str.equals(".")) break;
            brands.add(str);
        }

        System.out.println("Available Brands are : "+brands);

        return brands;
    }

    public   ArrayList<String> addCategories(String brand){
        ArrayList<String> categories = new ArrayList<String>();
        System.out.println("Enter Categories for brand  "+brand + " : ");
        while(scr.hasNext())
        {
            String cat = scr.nextLine();
            if(cat.equals(".")) break;
            categories.add(brand+"_"+cat);

        }
        System.out.println("Available Categories are :" + categories);
        return categories;
    }

    public   ArrayList<String> addSubCategories(String category){
        ArrayList<String> subcategories = new ArrayList<String>();
        System.out.println("Enter SubCategories for category  "+category+" : ");
        while(scr.hasNext())
        {
            String subcat = scr.nextLine();
            if(subcat.equals(".")) break;
            subcategories.add(subcat);

        }
//        System.out.println("Available SubCategories are :" + subcategories);
        return subcategories;
    }

    public  HashMap<String, ArrayList<String>> GetItems(String keyBrand)
    {
        Iterator<Map.Entry<String, HashMap<String, ArrayList<String>>>> it = items.entrySet().iterator();
        HashMap<String, ArrayList<String>> mapItems = new HashMap<String, ArrayList<String>>();
        boolean isKeyExists =false;
        while(it.hasNext())
        {
            //Iterate through the collection.
            Map.Entry<String,HashMap<String, ArrayList<String>>> entry = it.next();
           if (keyBrand == entry.getKey())
           {
               isKeyExists= true;
               mapItems = entry.getValue();
               System.out.println("This is the key which need to be searched in the items" + mapItems);
           }


        }
        return mapItems;
    }

   /* public static void main(String[] args) {

        Owner own = new Owner();
        HashSet<String> brnds = own.addBrands();
//        ArrayList<String> cats = addCategories();

        Iterator itr = brnds.iterator();
        String brand = "";
        while(itr.hasNext()){
             brand = itr.next().toString();
            ArrayList<String> cats = own.addCategories(brand);
            own.brandCategories.put(brand, cats);

            Iterator subitr = cats.iterator();
            ArrayList<String> subcats = new ArrayList<String>();

            String category = "";

            while(subitr.hasNext()){
                category = subitr.next().toString();
                subcats = own.addSubCategories(category);
                own.brandSubCategories.put(category,subcats);
            }

            own.items.put(brand, own.brandSubCategories);
            //System.out.println(items);

        }
        System.out.println("Below are the Items entered by Owner");
        System.out.println(own.brandSubCategories);
//        System.out.println(items);
*//*

        System.out.println("Enter brand name to view items : ");
        String brndname = scr.nextLine();

        if(brands.contains(brndname)){
            ArrayList<String> categories = brandCategories.get(brndname);
            System.out.println(brndname + " brand has " + categories);
            for(int i=0; i<categories.size();i++){
                ArrayList<String> subCategories = brandSubCategories.get(categories.get(i));
                System.out.println(brndname + " brand has " + subCategories + " under " + categories.get(i) + "category");
            }
        }
*//*

    }*/
}
