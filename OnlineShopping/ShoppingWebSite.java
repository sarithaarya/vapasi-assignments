import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;

public class ShoppingWebSite {

    public static void main(String[] args) {

        Owner own = new Owner();
        HashSet<String> brnds = own.addBrands();
//        ArrayList<String> cats = addCategories();

        Iterator itr = brnds.iterator();
        String brand = "";
        while(itr.hasNext()){
            brand = itr.next().toString();
            ArrayList<String> cats = own.addCategories(brand);
            own.brandCategories.put(brand, cats);

            Iterator subitr = cats.iterator();
            ArrayList<String> subcats = new ArrayList<String>();

            String category = "";

            while(subitr.hasNext()){
                category = subitr.next().toString();
                subcats = own.addSubCategories(category);
                own.brandSubCategories.put(category,subcats);
            }

            own.items.put(brand, own.brandSubCategories);
            //System.out.println(items);

        }
        System.out.println("Below are the Items entered by Owner");
        System.out.println(own.brandSubCategories);
//        System.out.println(items);


        System.out.println("Enter brand name to view items : ");
        Scanner scr = new Scanner(System.in);
        String brndname = scr.nextLine();
        System.out.println(brndname);

        User objUsr = new User();
        final ArrayList<String> cartItems = objUsr.addItemstoCart(brndname);
        System.out.println(cartItems);

       //ArrayList<String> str= own.brandCategories.keySet().contains();
       /*if(brands.contains(brndname)){
            ArrayList<String> categories = brandCategories.get(brndname);
            System.out.println(brndname + " brand has " + categories);
            for(int i=0; i<categories.size();i++){
                ArrayList<String> subCategories = brandSubCategories.get(categories.get(i));
                System.out.println(brndname + " brand has " + subCategories + " under " + categories.get(i) + "category");
            }
        }*/


    }
}
